#!/usr/bin/python3

from nicegui import ui, background_tasks 
from nicegui.events import ValueChangeEventArguments

import os
import platform
import time
import natsort

anz=ui.row()

konfigpfad="/home/upopp/python/ScheinwerferSwitch/konfiguration/"

sstelle = []

global send
send=[]

global switch
switch=[]
anlassen=[]

datei=os.listdir(konfigpfad)
dateien=[]

for d in datei:
    dpfad=konfigpfad + d
    with open(dpfad, "r") as da:
         try:
             ezeile = da.readlines()[0].rstrip()
             st=int(ezeile.rstrip().split(':')[0])
             di = str(int((st/6)+1))
             name = d.rstrip().split('.')[0]
             dateien.append(di+'-'+name)
             #pro Schweinwerfer werden 6 Werte der Liste fürs Senden angehaengt
             for s in range(6):
               send.append('0')
             switch.append(False)
             anlassen.append(False)

         except:
             di = '0'
dateien = natsort.natsorted(dateien)

for dat in range(len(dateien)):
    dateien[dat]=(dateien[dat].rstrip().split('-')[1] + '.conf')


schieber=['0', '0', '0', '0', '0', '0']
bezsch=['Rot', 'Grün', 'Blau', 'Bernstein', 'Weiß', 'Utraviolett']



def sw_para_einlesen(pfad):
    config = open(pfad, "r")
    konfig=[]
    i=0
    for line in config:
      i=i+1
      if i==1:
         start=int(line.rstrip().split(':')[0])
         end=int(line.rstrip().split(':')[1])
      else:
         line=line.rstrip().split('\n')[0]
         konfig.append(line)
    return(start,end,konfig)

def name(nummer):
    datei = dateien[nummer]
    name = datei.rstrip().split('/')[-1]
    name = name.rstrip().split('.')[0]
    return(name)

def aktualisieren(value: float, z: int) -> None: #aktuallisiert beim schieben 
                                                 #Schiebers den Wert und 
                                                 #testet den ganzen Scheinwerfer
    sstring=[]
    for h in range(len(send)):
        sstring.append('0')
    pos = int(z/6)
    pos=pos*6
    send[z] = value
    sstring[pos:(pos+6)] = send[pos:(pos+6)]
    for u in range(4):    #4 scheinwerfer insgesamt
        if anlassen[u]==True:
           al = u*6       #6 Schieber pro Scheinwerfer
           sstring[al:(al+6)] = send[al:(al+6)] 
    print(sstring)
    string=''
    for y in range(len(sstring)):
        string=string + str(sstring[y]) + ','
    string=string[:-1]
    befehl=('/usr/bin/ola_streaming_client -u 0 -d ' + string)
    os.system(befehl)

def schalter(value: float, v: int) -> None:
    anlassen[v]=value

with ui.tabs().classes('w-full center') as tabs:
    for i in range (len(dateien)):
         bez=(name(i))
         ui.tab(bez)

with ui.tab_panels(tabs.classes('w-full center'), value=bez):
    for i in range (len(dateien)):
        bez=(name(i))
        with ui.tab_panel(bez).classes('w-full center'):
             konf=konfigpfad + dateien[i]
             ui.label(bez + ': ' + konf).classes('w-full center')
             switch[i] = ui.switch('Scheinwerfer an lassen', on_change=lambda f,i=i: schalter(f.value, i))
             start,end,konfig=sw_para_einlesen(konf)
             ui.notify(konf)
             for j in range(6):
                ind = ((i*6)+j)
                send[ind]=konfig[j]
                schieber[j] = ui.slider(min=0, max=255, value=konfig[j], on_change=lambda e, i=ind: aktualisieren(e.value, i))
                ui.label(bezsch[j])
                ui.label().bind_text_from(schieber[j], 'value')

             async def speichern(x: int) -> None:
                  konfig = send[(x*6):((x*6)+6)]
                  startend=(str(x*6) + ':' + str((x*6)+5))
                  konfdatei=konfigpfad + dateien[x]
                  schreibe = open(konfdatei, 'w')
                  schreibe.write(startend + '\n')
                  for v in range(6):
                            schr = (str(konfig[v]) + '\n')
                            schreibe.write(schr)
                  schreibe.close
                  
 
             ui.button('Speichern', on_click=lambda _, w=i: background_tasks.create(speichern(w)))
   
             




ui.run(port=8081, reload=platform.system() != "Windows")

