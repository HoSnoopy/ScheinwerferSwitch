#!/usr/bin/python3
#coding: utf-8                                                                   
import cgi, cgitb
import os
import natsort



konfigpfad="/home/upopp/python/ScheinwerferSwitch/konfiguration/"
datei=os.listdir(konfigpfad)

dateien=[]

for d in datei:
    dpfad=konfigpfad + d
    with open(dpfad, "r") as da:
         try:
             ezeile = da.readlines()[0].rstrip()
             st=int(ezeile.rstrip().split(':')[0])
             di = str(int((st/6)+1))
             name = d.rstrip().split('.')[0]
             dateien.append(di+'-'+name)
             #pro Schweinwerfer werden 6 Werte der Liste fürs Senden angehaengt
             for s in range(6):
                send.append('0')
         except:
             di = '0'
dateien = natsort.natsorted(dateien)

print("Content-Type: text/html\n\n")
print('<html><head><title>Scheinwerferauswahl</title>') 
print('</head><body><h1>')

print('<form method="get" action="/cgi-bin/licht.py">')
print('<fieldset><legend>Scheinwerfer</legend>')
print('<table>')
for l in range((len(dateien))):
    if (l/2) == int((l/2)):
         if not l==0:
            print('</tr>')
         print('<tr>')
    print('<td> <input type="checkbox" name="' + str(l+1) + '" value="1">' + dateien[l] + '</td>')
print('<tr>')
print('<td> <input type="checkbox" name="' + str((len(dateien)+1)) + '" value="1">Alle</td>')
print('<td> <input type="checkbox" name="' + str((len(dateien)+2)) + '" value="1">Band</td>')
print('</tr>')
print('</table>')
print('<input type="submit" value="Abschicken" />')
print('</fieldset>')
print('</form>')
