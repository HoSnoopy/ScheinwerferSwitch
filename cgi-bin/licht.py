#!/usr/bin/python3
#coding: utf-8                                                                                
                                                                                              
import cgi, cgitb                         
import os
import time
import natsort

#Wenn nichts ausgewält ist, werden alle Schweinwerfer ausgeschaltet

#Konfigurationsdaeteien für die einzelnen Scheinwerfer:
#1. Zeile: Bereich des zu sendenden Arrays (jeweilige DMX-Kanäle)
#Folgende Zeilen: Werte der der DMX-Kanäle (in Fall hier: 
#Rot, Grün, Blau, Bernstein, Weiß, UV, also jeweils 6 Werte

konfigpfad="/home/upopp/python/ScheinwerferSwitch/konfiguration/"

band = [3, 5 , 6] #Schweinwerfernummern für die Band-Gruppierung


datei=os.listdir(konfigpfad)
send = [] 
dateien=[]

for d in datei:
    dpfad=konfigpfad + d
    with open(dpfad, "r") as da:
         try:
             ezeile = da.readlines()[0].rstrip()
             st=int(ezeile.rstrip().split(':')[0])
             di = str(int((st/6)+1))
             dateien.append(di+'-'+d)
             #pro Schweinwerfer werden 6 Werte der Liste fürs Senden angehaengt
             for s in range(6):
                send.append('0')
         except:
             di = '0' 
dateien = natsort.natsorted(dateien)
#print(send)

max = len(dateien)+1 # Anzahl der Auswahlmöglichkeiten auf der Maske 


#Werte aus Konfiguration einlesen
def sw_para_einlesen(pfad):
    config = open(pfad, "r")
    konfig=[]
    i=0
    for line in config:
      i=i+1
      if i==1:
         start=int(line.rstrip().split(':')[0])
         end=int(line.rstrip().split(':')[1])
      else:
         line=line.rstrip().split('\n')[0]
         konfig.append(line)
    return(start,end,konfig)

#welche Scheinwerfer sollen eingeschaltet werden?
#/cgi-bin/licht.py?1=1 => 1. Scheinwerfer wird eingeschaltet
form = cgi.FieldStorage()


#Abfragen, ob die Band angeklickt ist

try:
    info = str((form.getvalue(str(max+1))))
    if info == '1':
       bnd = True
    else:
       bnd = False
except:
    bnd = False


getpara = []

for devnr in range(len(dateien)+1):
     nr = devnr + 1
     try:
         info = str((form.getvalue(str(nr))))
         if info == '1':
            # getpara.append(1)
            appe = True
         else:
             #getpara.append(0)
             appe = False
     except:
         #getpara.append(0)
         appe = False
     
     try:
         for ba in range(len(band)):
            if nr == band[ba] and bnd == True: 
               appe = True  
     except:
         bnd = bnd 

     if appe == True:
         getpara.append(1)
     else:
         getpara.append(0)

#Hole im Falle einer auswahl die Konfigurationswerte und füge sie im 
#Sende-String bzw. erstmal ins Array ein     
for devnr in range(len(dateien)):
     nr = devnr + 1
# Scheinwerfer wird direkt oder durch den Knopf 'alle' ausgewählt
     if getpara[devnr] == 1 or getpara[(max-1)] == 1: 
         dateiname = dateien[devnr].rstrip().split('-')[1]
         start,end,konfig=sw_para_einlesen(konfigpfad + dateiname)
         send[start:end]=konfig


#Mache String aus dem Array
string = ''
for i in range(len(send)):
    string=string + send[i] + ','
string=string[:-1]

#print(string)

befehl=('/usr/bin/ola_streaming_client -u 0 -d ' + string)

#Problem: Der Enttec-OpenDMX-USB-Kontroller reagiert nicht bei jedem 
#Befehl, also schicke den Befehl 10x mit einer Pause von 0,2 Sekunden 
#dazwischen.
for i in range(10):
    os.system(befehl)
    time.sleep(0.2)

#Gebe HTML-Code raus und schicke User auf die HTML-Seite zurück.
     
print("Content-Type: text/html\n\n")                                                          
print("""                                                                                     
        <html><head><title>Scheinwerfereinstellen</title>                              
          <meta http-equiv="refresh" content="1; URL=/cgi-bin/scheinwerfer.py">
        </head>                                                                               
        <body>                                                                                
<center><h1>                                                                                  

                                                                                              
""")                                                                                          

print("</body></html>")
